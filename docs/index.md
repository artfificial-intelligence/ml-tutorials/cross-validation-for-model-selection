# 모델 선택을 위한 교차 검증 <sup>[1](#footnote_1)</sup>

> <font size="3">최적의 기계 학습 모델을 선택하기 위해 교차 검증을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./cross-validation.md#intro)
1. [교차 검증이란?](./cross-validation.md#sec_02)
1. [교차검증의 종류](./cross-validation.md#sec_03)
1. [Python에서 교차 검증 수행](./cross-validation.md#sec_04)
1. [교차검증의 장단점](./cross-validation.md#sec_05)
1. [마치며](./cross-validation.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 17 — Cross-Validation for Model Selection](https://levelup.gitconnected.com/ml-tutorial-17-cross-validation-for-model-selection-90009170b774?sk=1b978e04c8523ab2e62ed17b5098f6c4)를 편역하였습니다.
